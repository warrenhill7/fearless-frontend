function createCard(name, description, pictureUrl, startDate, endDate,subtitle) {
    return `
      <div class="card grid text-center shadow p-3 mb-5 bg-white rounded style="--bs-columns: 18; --bs-gap: .5rem;">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${subtitle}</h6>
          <p class="card-text">${description}</p>
          <div class="card-footer">${startDate}-${endDate}</div>
        </div>
      </div>
    `;
  }


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {

      const response = await fetch(url);

      if (!response.ok) {
         throw new Error("This is an Error")
        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();
        let index = 0
        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const name = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const starts = new Date(details.conference.starts);
            const ends = new Date(details.conference.ends);
            const startDate = starts.toLocaleDateString();
            const endDate = ends.toLocaleDateString();
            const subtitle = details.conference.location.name;
            const html = createCard(name, description, pictureUrl, startDate, endDate,subtitle);
            // const column = document.querySelector('.col');
            const column = document.querySelector(`#col-${index%3}`);
            column.innerHTML += html

            index++;
          }
        }

      }
    } catch (e) {
        console.log(e)
        const error = document.querySelector('.error-wrapper');
        error.innerHTML = `
        <div class="alert alert-danger" role="alert">
        A simple danger alert—check it out!
      </div>
        `
      // Figure out what to do if an error is raised
    }

  });
